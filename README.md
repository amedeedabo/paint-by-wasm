# Paint By Wasm

Turns an uploaded image into a Paint By Numbers template.

A learning project built with Svelte, Webpack, and Rust in Webassembly in a Web Worker.

Built using a a Gitlab CI pipeline and deployed with Gitlab Pages.

Copy of http://PBNify.com

A merge of `VictorGavrish/rust-wasm-worker-template` and `rust-wasm-webpack-template`.

Prerequisites
---
Rust
Node/NPM

Structure
---
* `/crate-wasm` contains the Rust code, which is turned into a wasm bundle
* `/www` contains the JS app

To run in dev mode, cd into `/www` and run `npm run start`.

To run in production mode, cd into `/www`, run `npm build`, and then `node server.js`.

Note that production mode is significantly faster than development mode.

`server.js` is included because for the near future the Wasm bundle mime type is not handled all that well by eg `python -m SimpleHTTPServer` and friends.
