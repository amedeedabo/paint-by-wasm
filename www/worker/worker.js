import("../../crate-wasm/pkg").then(wasm => {
  wasm.init();
  self.addEventListener("message", ev => {
    console.log("Web worker started.");
    const imageData = ev.data.image;
    const width = ev.data.width;
    const height = ev.data.height;
    const palette = ev.data.palette;
    const paletteBuf = new Uint8Array(palette.length * 3);
    for (let i=0; i< palette.length;i++) {
      paletteBuf[i*3] = palette[i]["r"];
      paletteBuf[i*3+1] = palette[i]["g"];
      paletteBuf[i*3+2] = palette[i]["b"];
    }
    try {
      console.log(`Web worker: calling Rust on ${width} x ${height} image with ${palette.length} palette colors`);
      wasm.fill_image(imageData, width, height, paletteBuf);
    } catch (err) {
      self.postMessage({ "type": "error", error: err.message });
    }
  });
}).catch(err=> {console.log("Error while importing wasm"); console.log(err)});
