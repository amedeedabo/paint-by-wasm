#![feature(test)]
///This benchmark measures the performance of the kmeans clustering.
///To run it you need to comment out the `cdylib` annotation in cargo.toml, and all
///of the #[wasm_bindgen] annotations in the project.

extern crate test;
extern crate rand;
extern crate paintworker;

#[cfg(test)]
mod tests {
    use paintworker::kmeans::*;
    use test::{Bencher, black_box};
    use rand::Rng;

    #[bench]
    fn bench_kmeans_1000(b: &mut Bencher) {
        let mut rng = rand::thread_rng();
        let points: Vec<Point> = (0..10_000).map(|_| Point(rng.gen(), rng.gen(), rng.gen())).collect();

        b.iter(|| {
                black_box(run(&points, 8, 10));
        });
    }
}

