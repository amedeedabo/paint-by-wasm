extern crate js_sys;
use js_sys::Math;
extern crate web_sys;
use web_sys::console;

use std::hash::{Hash, Hasher};
use std::mem;
use std::ops::{Add, Sub};

#[derive(Debug, PartialEq, PartialOrd, Copy, Clone)]
pub struct Point(pub f64, pub f64, pub f64);

fn sq(x: f64) -> f64 {
    x * x
}

impl Hash for Point {
    fn hash<H: Hasher>(&self, state: &mut H) {
        // Perform a bitwise transform, relying on the fact that we
        // are never Infinity or NaN
        let Point(x, y, z) = *self;
        let x: u64 = unsafe { mem::transmute(x) };
        let y: u64 = unsafe { mem::transmute(y) };
        let z: u64 = unsafe { mem::transmute(z) };
        x.hash(state);
        y.hash(state);
        z.hash(state);
    }
}

impl Add for Point {
    type Output = Point;

    fn add(self, other: Point) -> Point {
        Point(self.0 + other.0, self.1 + other.1, self.2 + other.2)
    }
}

impl Sub for Point {
    type Output = Point;

    fn sub(self, other: Point) -> Point {
        Point(self.0 - other.0, self.1 - other.1, self.2 - other.2)
    }
}

impl Eq for Point {}

fn dist(v: Point, w: Point) -> f64 {
    let delta_r = v.0 - w.0;
    let delta_g = v.1 - w.1;
    let delta_b = v.2 - w.2;
    let r_prime = (v.0 + w.0) /2.;
    (2. + r_prime/256.) * sq(delta_r) + 4. * sq(delta_g) + (2. + (255. - r_prime)/256.) * sq(delta_b)
}

fn avg(points: &[Point]) -> Point {
    let Point(x, y, z) = points.iter().fold(Point(0.0, 0.0, 0.0), |p, &q| p + q);
    let k = points.len() as f64;

    Point(x / k, y / k, z / k)
}

fn closest_idx(x: Point, ys: &[Point]) -> usize {
    let mut current_dist = 10000000000000000.;
    let mut current_idx = 0;
    for (idx, &y) in ys.iter().enumerate() {
        let  d = dist(x, y);
        if d < current_dist {
            current_dist = d;
            current_idx = idx
        }
    }
    current_idx
}

fn closest(x: Point, ys: &[Point]) -> Point {
    ys[closest_idx(x, ys)]
}

fn starting_centroids(points: &[Point], n: usize) -> Vec<Point> {
    let mut centroids: Vec<Point> = Vec::with_capacity(n);

    while centroids.len() < n {
        let new_point: Point = points[(Math::random() * points.len() as f64).floor() as usize];
        if !centroids.contains(&new_point) {
            centroids.push(new_point.clone());
        }
    }
    centroids
}
pub fn run(points: &[Point], n: u32, iters: u32) -> Vec<Point> {
    console::time_with_label("kmeans");
    let interval = (points.len() as f64 / n as f64).floor();
    let mut centroids = starting_centroids(points, n as usize);
    let mut groups = vec![Vec::<Point>::with_capacity(interval as usize); n as usize];

    for i in 0..iters {
        console::time_with_label(&format!("kmeans {}", i));
        groups.iter_mut().for_each(|g| g.clear());
        for x in points.iter() {
            let idx = closest_idx(*x, &centroids);
            groups[idx].push(*x);
        }
        centroids = groups
            .iter()
            .map(|g| avg(&g))
            .collect();
        console::time_end_with_label(&format!("kmeans {}", i));
    }
    let centroid_pts = centroids.iter().map(|&centroid| closest(centroid, points)).collect();
    console::time_end_with_label("kmeans");
    centroid_pts
}