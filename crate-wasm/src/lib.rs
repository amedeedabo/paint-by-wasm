extern crate serde_json;
extern crate web_sys;

use std::cmp::{min,max};
use std::collections::{VecDeque, BTreeMap};
use wasm_bindgen::prelude::*;
use web_sys::console;
use serde_json::json;
use serde::Serialize;
mod utils;
mod kmeans;
use kmeans::*;

#[cfg(debug_assertions)]
static kmeans_iterations :u32 = 8;
#[cfg(not(debug_assertions))]
static kmeans_iterations :u32 = 24;

macro_rules! log {
    ( $( $t:tt )* ) => {
        web_sys::console::log_1(&format!( $( $t )* ).into());
    }
}

pub struct Timer<'a> {
    name: &'a str,
}

impl<'a> Timer<'a> {
    pub fn new(name: &'a str) -> Timer<'a> {
        console::time_with_label(name);
        Timer { name }
    }
}

impl<'a> Drop for Timer<'a> {
    fn drop(&mut self) {
        console::time_end_with_label(self.name);
    }
}

#[wasm_bindgen]
extern {
    fn postMessage(object: &JsValue);
}

type vec2d = Vec<Vec<usize>>;

#[derive(Debug)]
pub struct RGB {
    r: u8,
    g: u8,
    b: u8,
}

#[wasm_bindgen]
pub fn init() {
    utils::init();
    console::log_1(&"Hello from WebAssembly!".into());
}

#[derive(Serialize, Debug)]
pub struct LabelLoc{
    value: usize,
	x: usize,
    y: usize,
    goodness: u64,
}

pub fn compute_palette(image: &[u8], width: usize, height: usize) -> Vec<u8> {
    let _timer = Timer::new("Worker::ComputePalette");
    let mut image_points: Vec<Point> = Vec::with_capacity(width * height);
    for r in 0..height {
        for c in 0..width {
            let image_idx = (r * width + c) * 4;
            image_points.push(Point(
                image[image_idx + 0] as f64,
                image[image_idx + 1] as f64,
                image[image_idx + 2] as f64,
            ));
        }
    }
    let palette_points = run(&image_points.as_slice(), 8, kmeans_iterations);
    let mut palette_rgb : Vec<u8> = Vec::new();
    for pt in palette_points.iter() {
        palette_rgb.push((pt.0 ) as u8);
        palette_rgb.push((pt.1 ) as u8);
        palette_rgb.push((pt.2 ) as u8);
    }
    postMessage(&JsValue::from_str(&json!({
                        "type": "status",
                        "status": "Done computing palette"
                    }).to_string()));
    postMessage(&JsValue::from_str(&json!({
                        "type": "palette",
                        "palette": palette_rgb,
                    }).to_string()));
    palette_rgb
}

#[wasm_bindgen]
pub fn fill_image(image: &[u8], width: usize, height: usize, palette_opt:&[u8]) {
    let _timer = Timer::new("Worker::Complete");

    //I don't know how to make a palette variable that is &[u8], and either palette_opt if it's not empty
    //or calls compute_palette() when it is.
    //I used to have let palette = if palette_opt.len() > 0 {..} else {compute_palette().as_slice()}
    //but it didn't pass the borrow checker.
    let mut palette= palette_opt.into();
    if  palette_opt.len() == 0 {
        palette = compute_palette(image, width, height);
    }
    let rgb_palette: Vec<RGB> = palette.chunks(3).map(|triple| {
        RGB { r: triple[0], g: triple[1], b: triple[2]}
    }).collect();

    postMessage(&JsValue::from_str(&json!({
                        "type": "status",
                        "status": "Quantizing image to palette"
                    }).to_string()));
    let mat: vec2d = imageDataToSimpMat(image, width, height, &rgb_palette);
    postMessage(&JsValue::from_str(&json!({
                        "type": "status",
                        "status": "Smoothing"
                    }).to_string()));
    let mut smoothed :vec2d = smooth(&mat, rgb_palette.len());
    postMessage(&JsValue::from_str(&json!({
                        "type": "status",
                        "status": "Getting regions"
                    }).to_string()));

    let labelLocs = getLabelLocs(&mut smoothed);
    postMessage(&JsValue::from_str(&json!({
                        "type": "result.labellocs",
                        "labellocs": labelLocs,
                    }).to_string()));
    postMessage(&JsValue::from_str(&json!({
                        "type": "status",
                        "status": "Flattening image to send back"
                    }).to_string()));
    let filled: Vec<u8> = smoothed.iter().flat_map(|usize_vec| usize_vec.iter().map(|us| *us as u8)).collect();
    postMessage(&JsValue::from_str(&json!({
                        "type": "status",
                        "status": "Drawing outline"
                    }).to_string()));
    let outlined: Vec<u32> = outline(&smoothed);
    postMessage(&JsValue::from_str(&json!({
                        "type": "result.outline",
                        "outline": outlined,
                    }).to_string()));
    postMessage(&JsValue::from_str(&json!({
                        "type": "result",
                        "filledImage": filled
    }).to_string()));
}

pub fn outline(mat: &Vec<Vec<usize>>) -> Vec<u32> {
    //just go straight for the 1d here, it's pretty easy
    let width = mat[0].len();
    let height = mat.len();
    let mut outline :Vec<u32> = Vec::new();
    for r in 0..(height-1) {
        for c in 0..(width-1) {
            if !(mat[r][c] == mat[r+1][c] && mat[r][c] == mat[r][c+1]) {
                outline.push((r* width + c) as u32);
            }
        }
    }
    outline
}

pub fn simpToMat(image: &[u8], width: usize, height: usize) -> vec2d {
    let _timer = Timer::new("Worker::simpToMat");
    let mut image2d = vec![vec![0; width]; height];
    for r in 0..height {
        for c in 0..width {
            image2d[r][c] = image[r * width + c] as usize;
        }
    }
    image2d
}

pub fn imageDataToSimpMat(image: &[u8], width: usize, height: usize, palette: &Vec<RGB>) -> vec2d {
    let _timer = Timer::new("Worker::imageDataToSimpMat");
    let mut nearest2d = vec![vec![0; width]; height];

    for r in 0..height {
        for c in 0..width {
            let image_idx = (r * width + c) * 4;

            let mut closest_idx = 0;
            let mut closest_dist = 1_000_000_000;
            for palette_idx in 0..palette.len() {
                let color = &palette[palette_idx];
                let dist = (color.r as i32 - image[image_idx+0] as i32) * (color.r as i32 - image[image_idx+0] as i32) +
                           (color.g as i32 - image[image_idx+1] as i32) * (color.g as i32 - image[image_idx+1] as i32) +
                           (color.b as i32 - image[image_idx+2] as i32) * (color.b as i32 - image[image_idx+2] as i32) ;
                if dist < closest_dist {
                    closest_dist = dist;
                    closest_idx = palette_idx;
                }
            }
            nearest2d[r][c] = closest_idx;
        }
    }
    nearest2d
    }

pub fn smooth(mat: &vec2d, num_colors: usize) -> vec2d {
    let _timer = Timer::new("Worker::smooth");
    const range : usize = 3;
    let mut smoothed = mat.clone();
    let height = mat.len();
    let width = mat[0].len();

    let mut counts = vec![0; num_colors];
    let mut best_val = 0;
    for r in 0..height {
        for i in 0..num_colors {
            counts[i] = 0;
        }
        for xx in 0..=range {
            for yy in r.saturating_sub(range)..=(r+range) {
                if xx < width && yy < height {
                    let val = mat[yy][xx];
                    counts[val] +=1;
                }
            }
        }
        for c in 0..width {
            if c > 0 {
                if c >= range {
                    for yy in r.saturating_sub(range)..=min(r+range, height-1) {
                        let val = mat[yy][c-range];
                        counts[val] -=1;
                    }
                }
                if c + range < width  {
                    for yy in r.saturating_sub(range)..=min(r+range, height-1) {
                        let val = mat[yy][c+range];
                        counts[val] +=1;
                    }
                }
            }
            let mut best_idx = 0;
            let mut best_count = 0;
            for (idx, count) in counts.iter().enumerate() {
                if *count > best_count {
                    best_count = *count;
                    best_idx = idx;
                }
            }
            smoothed[r][c] = best_idx;
        }
    }
    smoothed
}

pub fn sameCount(mat: &vec2d, start_x: usize, start_y: usize, incX: i32, incY: i32) -> u64 {
    let mut count = 0;
    let mut x = start_x;
    let mut y = start_y;
    let value = mat[y][x];
    while x >= 0 && x < mat[0].len() &&
          y >= 0 && y < mat.len() &&
          mat[y][x] == value {
        count += 1;
        x = (x as i32 + incX) as usize;
        y = (y as i32 + incY) as usize;
    }
    count - 1
}
//WIP:
//Store the lengths of each row and column for the region (which my be concave so one row might
//have many single strips)
//For each pixel calculate its distance from left * right * up * down by looking up these values
//I believe this thing is quartic is region size?
//for every pixel (r * c) we iterate through c times * r times to find the distance from top+bottom/
//left+right...
pub fn find_col_len(mat: &vec2d, start_x: usize, start_y: usize, value: usize) -> (usize, usize) {
    let mut y = start_y;
    let x = start_x;
    while y > 0 {
        if mat[y][x] == value {
            y-=1;
        } else {
            break;
        }
    }
    let a = y;
    y = start_y;
    while y < mat.len() - 1{
        if mat[y][x] == value {
            y+=1;
        } else {
            break;
        }
    }
    (a, y)
}
pub fn find_row_len(mat: &vec2d, start_x: usize, start_y: usize, value: usize) -> (usize, usize) {
    let mut x = start_x;
    let y = start_y;
    while x > 0 {
        if mat[y][x] == value {
            x-=1;
        } else {
            break;
        }
    }
    let a = x;
    x = start_x;
    while x < mat[0].len() - 1 {
        if mat[y][x] == value {
            x+=1;
        } else {
            break;
        }
    }
    (a, x)
}
pub fn getLabelLocFast(mat: &vec2d, xs: &Vec<usize>, ys: &Vec<usize>) -> LabelLoc {
    let _timer = Timer::new("Worker::GetLabelLocFast");
    // let mut row_sizes: BTreeMap<usize, Vec<(usize, usize)>> = BTreeMap::new();
    // let mut col_sizes: BTreeMap<usize, Vec<(usize, usize)>> = BTreeMap::new();
    let mut best_i = 0;
    let mut best :u64 = 0;
    let val = mat[ys[0]][xs[0]];
    for i in 0..xs.len() {
        let c = xs[i];
        let r = ys[i];
        let mut col_int = (0, 0);
        let mut row_int = (0, 0);
        // let mut col_size_entry = col_sizes.entry(c).or_insert(vec![]);
        // for col in col_size_entry.iter() {
        //     if col.0 < r && r < col.1 {
        //     log!("Pulling out memoize col int {}, {}", col.0, col.1);
        //         col_int = *col;
        //         break;
        //     }
        // }
        // if col_int == (0,0) {
            col_int = find_col_len(mat, c, r, val);
            // col_size_entry.push(col_int);
        // }
        // let mut row_size_entry = row_sizes.entry(r).or_insert(vec![]);
        // for row in row_size_entry.iter() {
        //     if row.0 < c && c < row.1 {
        //     log!("Pulling out memoized row int {}, {}", row.0, row.1);
        //         row_int = *row;
        //         break;
        //     }
        // }
        // if row_int == (0,0) {
            row_int = find_row_len(mat, c, r, val);
            // row_size_entry.push(row_int);
            // log!("Locfast: done computing interval lengths");
        // }
        // log!("have intervals for point r:{}, c:{} col {} -> {}, row {} -> {}", r, c, col_int.0, col_int.1, row_int.0, row_int.1);
        //x = 61, y =5
        //row_int = 60->5
        //cow_int = 0->6
        if col_int.0 == r || r == col_int.1 || row_int.0 == c || c == row_int.1 {
            continue;
        }
        let goodness: u64 = (((r - col_int.0) * (col_int.1 - r)) as u64 ) * (((c - row_int.0) * (row_int.1 - c)) as u64);
        if goodness > best {
            best = goodness;
            best_i = i;
        }
    }

    LabelLoc {
        value: mat[ys[best_i]][xs[best_i]],
        x: xs[best_i],
        y: ys[best_i],
        goodness: best,
    }
}
pub fn getCentroid (xs: &Vec<usize>, ys: &Vec<usize>) -> (usize, usize) {
    let mut sum_x : u64 = 0;
    let mut sum_y : u64 = 0;
    let num_pixs = xs.len();
    for i in 0..num_pixs {
        sum_x += xs[i] as u64;
        sum_y += ys[i] as u64;
    }
    ((sum_x / (num_pixs as u64)) as usize,(sum_y / (num_pixs as u64)) as usize)
}
pub fn getLabelLoc(mat: &vec2d, xs: &Vec<usize>, ys: &Vec<usize>) -> LabelLoc {
    let value = mat[ys[0]][xs[0]];
    let (cen_x, cen_y) =  getCentroid(xs, ys);
    if mat[cen_y][cen_x] == value {
        LabelLoc {
            value,
            x: cen_x,
            y: cen_y,
            goodness: 1
        }
    } else {
        getLabelLocFast(mat, xs, ys)
    }
}

pub fn getLabelLocs(mat: &mut vec2d) -> Vec<LabelLoc> {
    let _timer = Timer::new("Worker::GetLabelLocs");
    let mut covered = vec![vec![false; mat[0].len()]; mat.len()];

    let mut labelLocs: Vec<LabelLoc> = Vec::new();
    for r in 0..mat.len() {
        for c in 0..mat[0].len() {
            if covered[r][c] == false {
                let (xs, ys, value) = getRegion(mat, &mut covered, c, r);
                if xs.len() > 100 { //only add the region if it is 100 pixels large
                    let locs = getLabelLoc(&mat, &xs, &ys);
                    labelLocs.push(locs);
                } else { //remove the region and recolor it with a different value if it is too small.
                    // This assumes first pixel in list is topmost then leftmost of region.
                    let mut y = if xs.len() > 0 {ys[0]} else {r};
                    let x = if xs.len() > 0 {xs[0]} else {c};
                    let new_value = if y > 0 { //either get the value above if we're not in row 0
                         mat[y - 1][x]
                    } else { //Or the value in the region directly below our starting pixel.
                        while y < mat.len() -1 && mat[y][x] == value {
                            y+=1;
                        }
                        mat[y][x]
                    };
                    for i in 0..xs.len() {
                        mat[ys[i]][xs[i]] = new_value;
                    }
                }
            }
        }
    }
    labelLocs
}

pub fn getRegion (mat: &vec2d, cov: &mut Vec<Vec<bool>>, x: usize, y: usize) -> (Vec<usize>, Vec<usize>, usize) {

    let mut xs = Vec::new();
    let mut ys = Vec::new();

    let value = mat[y][x];
    let mut queue: VecDeque<(usize, usize)> = VecDeque::new();
    queue.push_back((x,y));

    let width = mat[0].len();
    let height = mat.len();
    while let Some(coord) = queue.pop_front() {
        let c0 = coord.0;
        let c1 = coord.1;
        if cov[c1][c0] == false && mat[c1][c0] == value {
            xs.push(coord.0);
            ys.push(coord.1);
            cov[c1][c0] = true;
            if c0 > 0 { queue.push_back((c0 - 1, c1)); }
            if c0 < mat[0].len() - 1 { queue.push_back((c0 + 1, c1)); }
            if c1 > 0 { queue.push_back((c0, c1 - 1)); }
            if c1 < mat.len() - 1 { queue.push_back((c0, c1 + 1)); }
        }
    }

    (xs, ys, value)
}